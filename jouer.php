<?php
include("config.sample.php");
include("utils.php");
?>
<!doctype html>
<html>
<head>
        <link rel="icon" type="image/png" href="pendu.png">
	<title>Pendu</title>
	<meta charset>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<div class="jeu">
<h1>Le pendu !</h1>
<?php
require_once 'fonctions.inc.php';

session_start();
if(isset($_POST['quitter'])) session_unset();
if(isset($_POST['rejouer'])) unset($_SESSION['mot']) ;

if (isset ($_POST['niveau']) && isset ($_POST['pseudo']))
{
if($_POST['niveau']>0 && $_POST['niveau']<4 && strlen($_POST['pseudo']) != 0)
{
$_SESSION['niveau']=(int)$_POST['niveau'] ;
$_SESSION['pseudo']=$_POST['pseudo'] ;
}
}

if(!isset($_SESSION['pseudo']))
{//1er formulaire : demande du pseudo et du niveau de jeu
$Html='
<form action="jouer.php" method="post">
<p><input type="radio" name="niveau" value ="1"/> facile
<input type="radio" name="niveau" value ="2"/> difficile
<input type="radio" name="niveau" value ="3"/> expert</p>
<p>Entre ton prénom (ou ton pseudo) :
<input type="text" name="pseudo"/> </p>
<p><input type="submit" value="Envoyer"/></p>
</form>';
}
else
{//Le pseudo et le niveau de jeu sont connus
if(!isset($_SESSION['mot']))
{
$Mot=MotATrouver($_SESSION['niveau']) ;
$_SESSION['mot']=$Mot;
$Longueur=strlen($Mot) ;
$_SESSION['longueur']=$Longueur ;
$_SESSION['liste']="" ;
$Reponse=str_repeat('-',$Longueur) ;
$Reponse[0]=$Mot[0] ;
$Reponse[$Longueur-1]=$Mot[$Longueur-1] ;
$_SESSION['reponse']=$Reponse ;

$_SESSION['coups_restants']=Init_Coups_Restants($_SESSION['niveau']) ;
}
elseif(isset($_POST['lettre']))
{
$lettre=strtoupper($_POST['lettre']) ;
if(Recherche_Chaine_Dans_Tableau($_SESSION['liste'],$lettre)===FALSE)
{
if(Actualise_Reponse($lettre)==0) $_SESSION['coups_restants']-- ;
}
}

if($_SESSION['coups_restants']==0)
{
$Html='
<p>'.$_SESSION['pseudo'].', perdu, tu es pendu !</p>
<p>C\'était le mot '.$_SESSION['mot'].'</p>
<p><img src="./PenduApp/pendu11.png"/></p>
<form action="jouer.php" method="post">
<p>
<input type="submit" name="rejouer" value="Rejouer"/>
<input type="submit" name="quitter" value="Quitter">
</p>
</form>' ;
}
elseif($_SESSION['mot']!=$_SESSION['reponse'])
{
$Image=sprintf('./PenduApp/pendu%002d.png',12-$_SESSION['coups_restants']) ;
$Html='
<p>'.$_SESSION['pseudo'].', il te reste '.$_SESSION['coups_restants'].' coups avant d\'être pendu !</p>
<p>Liste : '.Assemble_Tableau_Chaine($_SESSION['liste']).'</p>
<p>Mot à trouver :'.str_replace('-',' - ',$_SESSION['reponse']).'</p>
<form action="jouer.php" method="post">
<p>Propose une lettre :
<input type="text" name="lettre"/>
<input type="submit" value="Envoyer">
</p>
<p>
<input type="submit" name="quitter" value="Abandonner / Quitter">
</p>
</form>
<p><img src="'.$Image.'"/></p>' ;
}
else
{
$Html='
<p>'.$_SESSION['pseudo'].', bravo, c\'est gagné !</p>
<p>C\'était le mot '.$_SESSION['mot'].'</p>
<form action="jouer.php" method="post">
<p>
<input type="submit" name="rejouer" value="Rejouer"/>
<input type="submit" name="quitter" value="Quitter">
</p>
</form>' ;
}

}
echo $Html ;
?>

</div>
<div class="encadrer">
      <nav>
        <ul>
	  <a href="index.php">Home</a>
          <a href="joueur.php">[Joueurs]</a>
          <a class="active" href="mot.php">[Suggestion de Mots]</a>      
	</ul>
      </nav>
</div>
</body>
</html>
