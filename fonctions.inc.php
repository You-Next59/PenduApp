<?php
function MotATrouver($Niveau)
{
switch($Niveau)
{
case 1 :
$Mot_a_trouver=array("angle","banc","clef","coin","eau","ecole","lit","mur","porte","rampe","salle","salon",
"siege","sol","table","tapis","vitre","aller","lever","venir","assis","bas","haut","face","loin",
"pres","tard","tot","apres","avant","dans","de","sous","sur","stylo","mine","craie","pli","colle",
"boite","jeu","jouet","pion","cube","perle","chose","forme","carre","rond","pate","livre","image",
"album","titre","conte","page","ligne","mot","carte","appel","chaine","doigt","ecran","film","fois",
"foi","idee","liste","main","micro","nom","photo","point","pouce","radio","sens","trait","voix","zero",
"dire","faire","finir","lire","plier","poser","salir","fou","tenir","trier","ami","dame","droit","eleve",
"faute","fille","ordre","obeir","blond","brun","calme","punir","doux","grand","moyen","muet","noir","poli",
"roux","sage","sale","pont","pot","roue","sac","balle","boite","petit","sourd","litre","pluie","jouer","laver",
"nager","moins","peu","trop","arcs","barbe","botte","col","epee","fusil","gant","habit","jean","jupe","lacet",
"laine","linge","magie","paire","pied","poche","reine","robe","roi","ruban","tache","talon","tissu","veste",
"clair","court","lacer","fonce","joli","large","long","bien","mal","mieux","seau","tasse","trous","verre","tirer",
"vider","chaud","froid","sec","avion","bois","bout","bruit","clou","colle","fil","metal","metre","objet","outil",
"scie","vis","taper","dur","lisse","tordu","engin","feu","frein","fusee","gare","grue","moto","pneu","quai","train",
"wagon","garer","deux","blanc","bleu","casse","cinq","dix","gris","gros","huit","jaune","meme","neuf","poser","voler",
"abime","rouge","sept","seul","six","trois","un","vert","vite","vers","arret","barre","bord","bras","chute","coeur",
"corde","corps","cote","cou","coude","dos","fesse","filet","fond","genou","jambe","jeu","mains","mur","poing","pont",
"signe","singe","pas","pente","peur","pied","saut","sport","tete","tour","gener","epais","fort","rond","serre","rater",
"bande","bille","cage","cerf","coup","cour","eau","paix","ici","pelle","pompe","preau","rayon","sable","tas","tuyau",
"velo","file","rang","crier");
break;
case 2 :
$Mot_a_trouver=array("armoire","bureau","cabinet","carreau","chaise","classe","couloir","dossier","entrer","arreter","atterrir",
"etroit","presque","ampoule","bricolage","cabane","carton","crochet","ficelle","marteau","trouver","bouder","charger",
"morceau","moteur","peinture","pinceau","planche","platre","voiture","arracher","attacher","casser","coudre",
"detruire","ecorcher","enfiler","enfoncer","fabriquer","mesurer","percer","pincer","reparer","reussir","servir",
"adroit","difficile","facile","pointu","accident","camion","garage","panne","parking","pilote","virage","vitesse",
"conduire","demarrer","donner","ecraser","envoler","garder","manquer","partir","reculer","rouler","tendre","zigzag",
"ancien","dernier","deuxieme","pareil","premier","quatre","solide","dessus","autour","arriere","barreau","escalade",
"cerceau","chaise","cheville","cuisse","danger","doigts","echasses","echelle","epaule","equipe","hanche","milieu",
"numero","ongle","parcours","plongeoir","poignet","poutre","prise","riviere","escalader","gagner","bagarrer","battre",
"crocodile","roulade","pirouette","serpent","suivant","toboggan","trampoline","tunnel","ventre","accrocher","poursuivre",
"appuyer","arriver","baisser","balancer","bondir","bousculer","cogner","courir","danser","depasser","descendre",
"glisser","grimper","marcher","pattes","debout","monter","montrer","pencher","percher","perdre","ramper","respirer",
"revenir","sauter","soulever","suivre","tomber","traverser","groupe","immobile","souple","ensemble","jamais","toujours",
"souvent","bagarre","ballon","volant","chateau","course","echasse","flaque","pardon","partie","pedale","raquette",
"cacher","cracher","creuser","degonfler","dispute","empecher","galoper","hurler","jongler","lancer","pedaler","plaindre",
"escalier","etagere","exterieur","fenetre","interieur","lavabo","marche","matelas","meuble","mousse","tricycle",
"peluche","placard","plafond","poubelle","radiateur","rideau","robinet","serrure","serviette","sieste","retourner",
"silence","sommeil","sonnette","sortie","tableau","tiroir","toilette","amener","apporter","appuyer","montagne",
"attendre","bailler","coucher","dormir","eclairer","emmener","emporter","entrer","fermer","frapper","muscle",
"installer","ouvrir","presser","rester","sonner","sortir","absent","present","gauche","droite","voyage","trouer",
"debout","dedans","dehors","contre","derriere","devant","crayon","feutre","gomme","dessin","coloriage","sifflet",
"couleur","papier","feuille","cahier","carnet","carton","ciseaux","decoupage","pliage","affaire","casier","ecarter",
"caisse","trousse","domino","puzzle","modeler","tampon","histoire","magazine","enveloppe","etiquette",
"affiche","appareil","cassette","chanson","chiffre","contraire","ecriture","instrument","intrus","lettre",
"modele","musique","nombre","poster","prenom","question","tambour","telephone","television","trompette",
"chanter","chercher","choisir","chuchoter","coller","colorier","commencer","comparer","compter","construire",
"continuer","copier","couper","dechirer","decoller","decorer","decouper","demolir","dessiner","discuter",
"ecouter","ecrire","effacer","entendre","entourer","envoyer","fouiller","gouter","pleurer",
"regarder","remettre","repeter","repondre","sentir","souligner","tailler","terminer","toucher","travailler",
"attention","camarade","colere","copain","coquin","directeur","directrice","effort","enfant","fatigue",
"garcon","gardien","madame","maitre","maitresse","personne","retard","joueur","sourire","travail","aider",
"defendre","desobeir","distribuer","echanger","expliquer","gronder","obliger","partager","preter","priver",
"promettre","progres","quitter","raconter","expliquer","refuser","separer","curieux","different","enerver","gentil",
"jaloux","nouveau","propre","serieux","tranquille","arrosoir","assiette","bateau","bouchon","bouteille","bulles",
"canard","casserole","cuillere","cuvette","douche","gouttes","moulin","poisson","plastique","saladier","tablier",
"agiter","amuser","arroser","attraper","avancer","baigner","barboter","boucher","bouger","deborder","doucher",
"eclabousser","essuyer","envoyer","couler","partir","flotter","gonfler","inonder","melanger","mouiller","pleuvoir",
"plonger","pousser","pouvoir","presser","recevoir","remplir","secher","serrer","souffler","tourner","tremper",
"verser","vouloir","amusant","humide","mouille","moitie","autant","beaucoup","encore","plusieurs","anorak",
"bagage","baguette","bonnet","bouton","bretelle","cagoule","casque","ceinture","chapeau","chausson","chaussure",
"chemise","cigarette","collant","couronne","cravate","culotte","echarpe","fleche","lunettes","magicien","maillot",
"manche","manteau","mouchoir","moufle","pantalon","prince","pyjama","semelle","soldat","sociere","taille","tricot",
"uniforme","valise","vetement","changer","chausser","couvrir","deguiser","enlever","habiller","porter","ressembler",
"imiter","laisser","mettre","montrer","ouvrir","parler","peindre","prendre","preparer","ranger","reciter",
"proteger","saigner","siffler","surveiller","trainer","trouver","mechant");
break;
case 3 :
$Mot_a_trouver=array("maternelle","tableaux","tabouret","rechauffer","coloriage","rayure","peinture","pinceau",
"cartable","bibliotheque","dictionnaire","catalogue","enveloppe","etiquette","alphabet","appareil","camescope",
"difference","magnetoscope","orchestre","ordinateur","telecommande","xylophone","recommencer","travailler",
"directrice","mensonge","progresser","expliquer", "handicape", "inseparable", "entonnoir", "renverser","conceptuelles",
"interessant", "transparent", "casquette", "chaussette", "deshabiller","multicolore","aiguille","elastique",
"marionnette","tournevis","maladroit","accident","aeroport","helicoptere","disparaitre","transporter","troisieme",
"acrobate","escabeau","gymnastique","passerelle","equilibre","quarante-deux","remplacer","transpirer","dangeureux",
"balancoire","bicyclette","ecureuil","recreation","arbalete","ampoule","brouette","brigadier","clochard","cariole",
"esthetique","franchise","genouillere","goudron","graduellement","habitation","haricot","habillement","habilite",
"immobilier","irrealisable","interdiction","juxtaposition","journalier","journaliste","kangourou","claquantes",
"kinésitherapeute","kilogramme","kerosene","luminosite","legionnaire","lieutenant","miserabilisme","clignotements",
"missionnaire","monarchique","monarchisme","neoprene","nicotine","nomenclature","nucleaire","antinomique",
"observation","objection","olympique","prisonnier","paleonthologue","palmeraie","pancreatique","accoutumés",
"quadrangulaire","quadrilatere","quadriceps","naturelles","supprimer","quinquagenaire","amnesiaques", "necropoles",
"radiocommunication","radioactivite","recidiviste","reciproquement","redoublement","serpentin","saisissant",
"scrupulesement","saltimbanque","scientifique","supernova","traduction","traceologie","tragedie","traineau",
"triceratops","unanimement","centrifugeuse","uniloculaire","ustensile","uppercut","urbanisme","utopisme","concassage",
"vaccination","vagabondage","verticalement","vitrificateur","waterproof","wagonnet","wattmetre","xylophone","collaborateurs",
"xanthophyle","yoghourt","yaourt","yemenite","yaourtiere","yakusa","yougoslave","zenithal","zingueur","zombie","aubergines",
"zezaiement","zodiaque","zoulou","zygomatique","informatique","numerique","baleines","arboricoles", "plantureux",
"imposteur","facetie","divulguer","concision","loquaces","sobriete", "melancoliques","alambique", "aliena","allegorie",
"augustes","belitre","bichette","borborygme","cabales","catimini","narguille","compulsifs","detracteurs","diantre","tapinois",
"endiguer","eructer","esoterique","idoine","inanite","ineffable","irrevencieux","laconique","litote","loquacite","marshmallow",
"megalomanie","nutritif","obsolescence","obsoletes","papelard","patelin","prostituée","poissarde","polymorphie","distilleries",
"psychotrope","redondantes","subliminal","taciturnes","trivialite","vacuite","vaudou","antillais","facile","aquarium",
"silencieuses","mobiliers","creperies","football","kamikazes","tracteurs","heliocoidale","chansonnier","ascensions","orthodoxes",
"hydroliques","maisonettes","abasourdis","abattons","abjectes","abjuriez","abondaient","abstinents","academiques","raccourcis",
"acheminons","automatismes","chromosomes","chuchotee","cicatrisons","cimenterie","cisaillements","citriques","clairvoyants",
"climatologues","cliquettements","coaguler","coaxiaux","pokemon","colombiers","commandées","compositrices","comprendriez",
"concrètement","confitures","conformistes","anarchistes","consolider","consommateurs","construiront","consultables","dissolvants",
"divulgatrices","dorenavant","douloureuses","dreadnought","helvetes","justiciers","convalescence","bourgeonnement","contemporaine");
}
//srand((double)microtime()*1000000);
/* Depuis PHP 4.2.0, nous n'avons plus besoin d'initialiser le générateur de nombres aléatoires avec srand() ou mt_srand()
* car c'est fait automatiquement.
*/
$Mot=strtoupper($Mot_a_trouver[rand(0,sizeof($Mot_a_trouver)-1)]);
return $Mot ;
}

function Init_Coups_Restants($Niveau)
{
return 13;
}

function Recherche_Chaine_Dans_Tableau($TabChaine, $Chaine)
{
$i=0 ;
while($Chaine!==$TabChaine[$i] && $i<sizeof($TabChaine))
{
$i++;
}
if($i==sizeof($TabChaine))
{
return FALSE;
}
else
{
return $i;
}
}

function Assemble_Tableau_Chaine($TabChaine)
{
$Chaine="" ;
if(sizeof($TabChaine)!=0)
{
for($i=0 ; $i<sizeof($TabChaine)-1 ; $i++)
{
$Chaine.=$TabChaine[$i].", " ;
}
$Chaine.=$TabChaine[$i] ;
}
return $Chaine ;
}

function Actualise_Reponse($Lettre)
{
$i=0 ;
$NbLettre=0 ;
do
{
$i=strpos($_SESSION['mot'], $Lettre, $i+1) ;
if($i>0 && $i<($_SESSION['longueur'])-1)
{
$_SESSION['reponse'][$i]=$Lettre ;
$NbLettre++ ;
}

}
while($i!==FALSE) ;
/* Ajouter $Lettre à la liste… */
$_SESSION['liste'][]=$Lettre ;
sort($_SESSION['liste']) ;
return $NbLettre ;
}
?>
