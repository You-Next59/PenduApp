<!doctype html>
<html>
<head>
	<link rel="icon" type="image/png" href="pendu.png">
	<title>Modif joueur</title>
	<meta charset>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<div class="encadrer">
      <nav>
        <ul>
          <li><a class="active" href="joueur.php">Joueurs</a></li>
          <li><a href="mot.php">Mots</a></li>
        </ul>
      </nav>

      <form action="update_nom.php" method="GET">
         <label for="joueur">Modifier nom du joueur par :</label>
         <input name="joueur" type="text" placeholder="Votre nouveau nom" tabindex="1" autocomplete="off">
         <input type="hidden" name="id" value="<?php echo $_GET["id"]; ?>">
         <input type="submit">
      </form>
  
<a href="joueur.php">Retour</a>
</div>
</body>
</html>
