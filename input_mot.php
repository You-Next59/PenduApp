<!doctype html>
<html>
<head>
	<link rel="icon" type="image/png" href="pendu.png">
	<title>Modif mot</title>
	<meta charset>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<div class="encadrer">
      <nav>
        <ul>
          <li><a href="joueur.php">Joueurs</a></li>
          <li><a class="active" href="mot.php">Mots</a></li>
        </ul>
         <div class="home_b">
            <a href="index.php">
            </a>
         </div>
      </nav>

      <form action="update_mot.php" method="GET">
         <label for="mot">Modifier mot :</label>
         <input name="mot" type="text" placeholder="Votre nouveau mot" tabindex="1" autocomplete="off">
         <input type="hidden" name="id" value="<?php echo $_GET["id"]; ?>" >
         <input type="submit">
      </form>
<a href="mot.php">Retour</a>
</div>
</body>
</html>
